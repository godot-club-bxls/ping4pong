extends Node2D


func _input(event):
	
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_UP:
			$palet.color = Color(0,1,0)
			$palet.length += 50
		elif event.scancode == KEY_DOWN:
			$palet.color = Color(1,0,0)
			$palet.length -= 50
		elif event.scancode == KEY_H:
			$palet.hit = true
		elif event.scancode == KEY_E:
			$palet.emit = true
