extends RigidBody2D

export (float,0,100) var radius:float = 20
export (bool) var is_server:bool = false

export (bool) var plus:bool = false setget set_plus
export (float,0,10) var plus_duration:float = 2

export (bool) var fire:bool = false setget set_fire
export (float,0,10) var fire_duration:float = 2

func set_plus(b:bool):
	plus = false
	if !is_server:
		plus_time = plus_duration

func set_fire(b:bool):
	fire = false
	if !is_server:
		fire_time = fire_duration
	
var fire_time:float = 0
var plus_time:float = 0

func _ready():
	$collision.shape.radius = radius
	$display.scale = (Vector2.ONE * radius * 2) / $display.texture.get_size()
	if is_server:
		remove_child($plus)
		remove_child($fire)
	else:
		$plus.emitting = false
		$plus.process_material.emission_sphere_radius = radius
		$fire.emitting = false
		$fire.process_material.emission_sphere_radius = radius

func _process(delta):
	if !is_server:
		var dir:Vector2 = linear_velocity.normalized() * -1
		$plus.process_material.direction = Vector3( dir.x, dir.y, 0 )
		$fire.process_material.direction = Vector3( dir.x, dir.y, 0 )
		
		if fire_time > 0:
			fire_time -= delta
			$fire.emitting = fire_time > 0
		
		if plus_time > 0:
			plus_time -= delta
			$fire.emitting = plus_time > 0
