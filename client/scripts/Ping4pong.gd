extends Node2D

const SERVER_IP = "127.0.0.1"
#const SERVER_IP = "109.89.100.173"
const SERVER_PORT = 31400
var peer
func _ready():
	peer = NetworkedMultiplayerENet.new()
	peer.create_client(SERVER_IP, SERVER_PORT)
	get_tree().set_network_peer(peer)
	#warning-ignore:return_value_discarded
	get_tree().connect("connection_failed", self, "_on_server_connection_failed")
	#warning-ignore:return_value_discarded
	get_tree().connect("server_disconnected", self, "_on_server_disconnected")
	get_tree().connect("connected_to_server", self, "_on_server_connected")

func _on_server_disconnected():
	terminateNetwork()

func _on_server_connection_failed():
	terminateNetwork()

func terminateNetwork():
	get_tree().set_network_peer(null)
	get_tree().quit()

var my_id
var border
var player_scene = preload("res://objects/palet.tscn")
var label_scene = preload("res://objects/pointsLabel.tscn")
#[right][tornado radius=0.5 freq=5][b]0[/b][/tornado][/right]

remote func player_connected(connected_player_id, myself, border, position, rotation, length, color):
	print(str(connected_player_id) + " - " + str(position))
	if(myself):
		my_id = str(connected_player_id)
		self.border = border
		match self.border:
			0:
				self.rotation = PI
				self.position = get_viewport().size
			1:
				self.rotation = PI*0.5
				self.position.x = get_viewport().size.x
			3:
				self.rotation = PI*-0.5
				self.position.y = get_viewport().size.y
			_:
				pass
	var new_player = player_scene.instance()
	var new_label = label_scene.instance()
	new_label.modulate = color
	new_player.name = str(connected_player_id)
	new_player.set_meta("label", new_label)
	new_label.set_position(position.normalized()*get_viewport().size)
	new_label.set_rotation_degrees(rotation)
	new_player.set_position(position)
	new_player.set_rotation_degrees(rotation)
	new_player.length = length
	new_player.color = color
	$PlayersContainer.add_child(new_player)
	add_child(new_label)

remote func update_position(peer_id, position):
	var player = $PlayersContainer.get_node_or_null(str(peer_id))
	if(player != null):
		player.position = position
	
remote func update_ball(position, linear_velocity):
#	print(str(position) + " - " +str(linear_velocity))
	$Ball.position = position
	$Ball.linear_velocity = linear_velocity

func _input(ev):
	if ev is InputEventKey:
		if ev.scancode == KEY_LEFT:
			rpc_id(1, "move_me", my_id, true, ev.pressed)
		elif ev.scancode == KEY_RIGHT:
			rpc_id(1, "move_me", my_id, false, ev.pressed)

