extends Node2D

export (bool) var is_server:bool = false

export (float,0,200) var width_physic:float = 10
export (float,0,200) var width_display:float = 10
export (float,0,200) var length:float = 20 setget set_length
export (float,0,200) var length_min:float = 20
export (float,0,500) var length_speed:float = 250

export (Color) var color:Color = Color(1,1,1,1) setget set_color
export (float,0,500) var color_speed:float = 10

export (bool) var hit:bool setget set_hit
export (float,0,100) var hit_anim_frequency:float = 80
export (float,0,10) var hit_anim_duration:float = 2

export (bool) var emit:bool = false setget set_emit
export (float,0,10) var emit_duration:float = 2

func set_length(f:float):
	if f < length_min:
		f = length_min
	if initialised:
		target_length = f
	else:
		length = f

func set_color(c:Color):
	if initialised:
		target_color = c
	else:
		color = c

func set_hit(b:bool):
	hit = false
	if b:
		hit_accu = 0
		hit_time = hit_anim_duration
	else:
		hit_time = 0

# warning-ignore:unused_argument
func set_emit(b:bool):
	emit = false
	if b:
		emit_time = emit_duration
	else:
		emit_time = 0

var initialised:bool = false
var target_color:Color = Color()
var target_length:float = 0
var hit_accu:float = 0
var hit_time:float = 0
var emit_time:float = 0

func _ready():
	
	if !is_server:
		remove_child( $physics )
	else:
		$physics/shape.shape.radius = width_physic
		$physics/shape.shape.height = length
		remove_child( $boom )
	$rect.color = color
	$rect.scale.x = width_display
	$rect.scale.y = length
	if !is_server:
		$boom.emitting = false
#		$boom.process_material.emission_box_extents.x = $rect.scale.x *.5
#		$boom.process_material.emission_box_extents.y = $rect.scale.y *.5
	
	target_color = color
	target_length = length
	
	initialised = true
		
func _process(delta):
	
	if !is_server:
		if hit_time > 0:
			hit_time -= delta
			if hit_time < 0:
				hit_time = 0
			hit_accu += delta
			$rect.position.x = sin( hit_accu * hit_anim_frequency ) * 3 * (hit_time/hit_anim_duration)
			
		if color != target_color:
			color = lerp( color, target_color, delta * color_speed )
			$rect.color = color
		
		if emit_time > 0:
			emit_time -= delta
			$boom.emitting = emit_time > 0
	
	if length != target_length:
		if length < target_length:
			length += length_speed * delta
			if length > target_length:
				length = target_length
		else:
			length -= length_speed * delta
			if length < target_length:
				length = target_length
		if is_server:
			$physics/shape.shape.height = length
		else:
			$boom.process_material.emission_box_extents.x = $rect.scale.x *.5
			$boom.process_material.emission_box_extents.y = $rect.scale.y *.5
		$rect.scale.y = length
