extends Control

onready var vp_size:Vector2 = get_viewport().size
onready var ping = $PING
onready var four = $FOUR
onready var pong = $PONG

var mouse = Vector2()

func _ready():
	var title_font = DynamicFont.new()
	title_font.font_data = load("res://fonts/TINY5x3-140.otf")
	title_font.size = 148
	ping.set("custom_fonts/font",title_font)
	ping.rect_rotation = -90
	ping.rect_position.x = 10
	ping.rect_position.y = vp_size.y -15
	four.set("custom_fonts/font",title_font)
	four.rect_position.x = 15
	four.rect_position.y = 10
	pong.set("custom_fonts/font",title_font)
	pong.rect_position.x = vp_size.x/2 +45
	pong.rect_position.y = 10
	var dynamic_font = DynamicFont.new()
	dynamic_font.font_data = load("res://fonts/whois-mono.ttf")
	dynamic_font.size = 50
	$Connect.set("custom_fonts/font", dynamic_font)
	var norm_styl:StyleBoxFlat = StyleBoxFlat.new()
	norm_styl.bg_color = Color(0.5,0.6,0.8,0)
	norm_styl.content_margin_top = 12
	norm_styl.content_margin_right = 12
	norm_styl.content_margin_bottom = 12
	norm_styl.content_margin_left = 12
	$Connect.set("custom_styles/normal",norm_styl)
	var hover_styl:StyleBoxFlat = StyleBoxFlat.new()
	hover_styl.bg_color = Color(0,0,0,1)
	hover_styl.content_margin_top = 12
	hover_styl.content_margin_right = 12
	hover_styl.content_margin_bottom = 12
	hover_styl.content_margin_left = 12
	$Connect.set("custom_styles/hover",hover_styl)
	$Connect.visible = false
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$Connect.rect_position.x = mouse.x-$Connect.rect_size.x/2
	$Connect.rect_position.y = mouse.y-$Connect.rect_size.y/2
#	pass

func _input(event):
	if event is InputEventMouseMotion:
		print("Mouse Motion at: ", event.position)
		mouse = event.position
		$Connect.visible = true

func _on_Connect_pressed():
	pass # Replace with function body.
