extends Control

onready var vp_size:Vector2 = get_viewport().size
onready var score_label = $Score
onready var score_number = $ScoreNumber

export (int) var score = 0

func _ready():
	var title_font = DynamicFont.new()
	title_font.font_data = load("res://fonts/TINY5x3-140.otf")
	title_font.size = 40
	score_label.set("custom_fonts/font",title_font)
	score_label.rect_position.x = vp_size.x/2-score_label.rect_size.x/2 -50
	var dynamic_font = DynamicFont.new()
	dynamic_font.font_data = load("res://fonts/whois-mono.ttf")
	dynamic_font.size = 30
	score_number.set("custom_fonts/font",dynamic_font)
	score_number.margin_top = 12
	score_number.rect_position.x = vp_size.x/2-score_number.rect_size.x/2 +45
	score_number.rect_position.y = 12


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	score_number.text = str(score)
#	pass
