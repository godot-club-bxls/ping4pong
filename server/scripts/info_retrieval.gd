tool

extends Node2D

export (NodePath) var p_path = "" setget set_p_path
export (NodePath) var b_path = "" setget set_b_path

var initialised:bool = false
var valid:bool = false
var p:Node2D
var b:Node2D

func set_p_path(np:NodePath):
	p_path = np
	initialised = false

func set_b_path(np:NodePath):
	b_path = np
	initialised = false

func _process(delta):
	if !initialised:
		valid = false
		initialised = true
		p = get_node( p_path )
		b = get_node( b_path )
		valid = b != null && p != null
	if valid:
		var p_up = Vector2(0,1).rotated(p.get_global_transform().get_rotation())
		var b_up = Vector2(0,1).rotated(b.get_global_transform().get_rotation())
		
		var b_relative = p.to_local( b.get_global_transform().get_origin() )
		var b_distance = b_relative.length()
		b_relative = b_relative.normalized()
		
		var towards = b_relative.dot( b_up ) * -1
		var right_relative = Vector2(b_relative.y, -b_relative.x)
		var right = b_up.dot( right_relative ) * -1
		
		var s:String = ""
		s += "towards me: " + str(towards) + "\n"
		s += "right/left: " + str(right) + "\n"
		s += "distance: " + str(b_distance) + "\n"
		$debug.set_text( s )
		
