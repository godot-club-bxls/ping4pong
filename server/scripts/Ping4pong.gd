extends Node2D

export (float,1,200) var player_length:float = 50
export (float,1,200) var player_width:float = 10

const Border = {
	UP = 0,
	RIGHT = 1,
	BOTTOM = 2,
	LEFT = 3
}

############################################################
#	SERVER CONSTANTS
const SERVER_IP = "127.0.0.1"
const SERVER_PORT = 31400
const MAX_PLAYERS = 4
var peer

onready var ballNode = $Ball.duplicate()
var currentBall

func _ready():
	$Borders/Area2DUP.connect("body_entered", self, "_on_Area2DUP_body_entered")
	$Borders/Area2DRIGHT.connect("body_entered", self, "_on_Area2DRIGHT_body_entered")
	$Borders/Area2DBOTTOM.connect("body_entered", self, "_on_Area2DBOTTOM_body_entered")
	$Borders/Area2DLEFT.connect("body_entered", self, "_on_Area2DLEFT_body_entered")
	
	$Borders/Area2DRIGHT/CollisionShape2D.position.x = 50 + OS.get_window_size().x
	$Borders/Area2DBOTTOM/CollisionShape2D.position.y = 50 + OS.get_window_size().y
	
	$Borders/Area2DUP.collision_layer = 0
	$Borders/Area2DRIGHT.collision_layer = 0
	$Borders/Area2DBOTTOM.collision_layer = 0
	$Borders/Area2DLEFT.collision_layer = 0
	$Borders/Area2DUP.collision_mask = 0
	$Borders/Area2DRIGHT.collision_mask = 0
	$Borders/Area2DBOTTOM.collision_mask = 0
	$Borders/Area2DLEFT.collision_mask = 0
	
	peer = NetworkedMultiplayerENet.new()
	peer.create_server(SERVER_PORT, MAX_PLAYERS)
	get_tree().connect('network_peer_connected', self, '_on_player_connected')
	get_tree().connect('network_peer_disconnected', self, '_on_player_disconnected')
	get_tree().set_network_peer(peer)
	get_tree().set_meta("network_peer", peer)
	$Ball.position = OS.get_window_size()/2
	ballNode.position = OS.get_window_size()/2
	currentBall = $Ball

var nextSpot = Border.UP
var player_scene = preload("res://objects/palet.tscn")
func instanciatePlayer(peer_id):
	var new_player = player_scene.instance()
	new_player.is_server = true
	new_player.length = player_length
	new_player.width_display = player_width
	new_player.width_physic = player_width
	
	# Color.from_hsv(0.58, 0.5, 0.79, 0.8) # Equivalent to HSV(210, 50, 79, 0.8)
	new_player.color = Color.from_hsv( (randi()%13)/12.0, 0.45, 0.65, 1.0)

	new_player.set_network_master(get_tree().get_network_unique_id())
	new_player.set_name(str(peer_id))
	match nextSpot:
		Border.UP:
			new_player.set_position(OS.get_window_size()*Vector2(0.5,0)+Vector2(0,new_player.width_display*2))
			new_player.set_rotation_degrees(90)
			$Borders/Area2DUP/CollisionShape2D/StaticBody2D.collision_layer = 0
			$Borders/Area2DUP.collision_layer = 1
		Border.RIGHT:
			new_player.set_position(OS.get_window_size()*Vector2(1,0.5)+Vector2(-new_player.width_display*2,0))
			new_player.set_rotation_degrees(180)
			$Borders/Area2DRIGHT/CollisionShape2D/StaticBody2D.collision_layer = 0
			$Borders/Area2DRIGHT.collision_layer = 1
		Border.BOTTOM:
			new_player.set_position(OS.get_window_size()*Vector2(0.5,1)+Vector2(0,-new_player.width_display*2))
			new_player.set_rotation_degrees(-90)
			$Borders/Area2DBOTTOM/CollisionShape2D/StaticBody2D.collision_layer = 0
			$Borders/Area2DBOTTOM.collision_layer = 1
		Border.LEFT:
			new_player.set_position(OS.get_window_size()*Vector2(0,0.5))
			$Borders/Area2DLEFT/CollisionShape2D/StaticBody2D.collision_layer = 0
			$Borders/Area2DLEFT.collision_layer = 1
	new_player.set_meta("border", nextSpot)
	new_player.set_meta("move", Vector2(0,0))
	nextSpot = pickNextAvailableSpot()
	return new_player
	
func pickNextAvailableSpot():
	var spot = 0
	var spotUnnavailable = true
	while(spotUnnavailable):
		spotUnnavailable = false
		spot+=1
		for player in $PlayersContainer.get_children():
			if(player.get_meta("border") == spot):
				spotUnnavailable = true
	return spot

func _on_player_connected(peer_id):
	print(str("Player Connected ", peer_id, "."))
	#Send player just connected, all the players that are on the server already
	for player in $PlayersContainer.get_children():
		rpc_id( peer_id, "player_connected", player.get_name(), false, 0, player.get_position(), player.get_rotation_degrees(), player.length, player.color)
	var new_player = instanciatePlayer(peer_id)
	#Send other players on the server the player that just connected
	for player in $PlayersContainer.get_children():
		rpc_id( int(player.get_name()), "player_connected", peer_id, false, 0, new_player.get_position(), new_player.get_rotation_degrees(), new_player.length, new_player.color)
	$PlayersContainer.add_child(new_player)
	#Send player just connected himself so that he can initialize
	rpc_id( peer_id, "player_connected", peer_id, true, new_player.get_meta("border"), new_player.get_position(), new_player.get_rotation_degrees(), new_player.length, new_player.color)

func _on_player_disconnected(peer_id):
	print(str("Player Disconnected ",peer_id,"."))
	var disconectedPlayer = $PlayersContainer.get_node(str(peer_id))
	var freeSpot = disconectedPlayer.get_meta("border")
	match freeSpot:
		Border.UP:
			$Borders/Area2DUP/CollisionShape2D/StaticBody2D.collision_layer = 1
			$Borders/Area2DUP.collision_layer = 0
		Border.RIGHT:
			$Borders/Area2DRIGHT/CollisionShape2D/StaticBody2D.collision_layer = 1
			$Borders/Area2DRIGHT.collision_layer = 0
		Border.BOTTOM:
			$Borders/Area2DBOTTOM/CollisionShape2D/StaticBody2D.collision_layer = 1
			$Borders/Area2DBOTTOM.collision_layer = 0
		Border.LEFT:
			$Borders/Area2DLEFT/CollisionShape2D/StaticBody2D.collision_layer = 1
			$Borders/Area2DLEFT.collision_layer = 0
	disconectedPlayer.queue_free()
	nextSpot = freeSpot

remote func move_me(peer_id, left, pressed):
	if(!pressed):
		$PlayersContainer.get_node(str(peer_id)).set_meta("move", Vector2(0,0))
		return
	var updateVector
	match $PlayersContainer.get_node(str(peer_id)).get_meta("border"):
		Border.UP:
			updateVector = Vector2(1,0)
		Border.RIGHT:
			updateVector = Vector2(0,1)
		Border.BOTTOM:
			updateVector = Vector2(-1,0)
		Border.LEFT:
			updateVector = Vector2(0,-1)
	if(left):
		$PlayersContainer.get_node(str(peer_id)).set_meta("move", updateVector)
	else:
		$PlayersContainer.get_node(str(peer_id)).set_meta("move", -updateVector)

func _process(delta):
	for player in $PlayersContainer.get_children():
		var new_position = player.position + player.get_meta("move")*delta*currentBall.MIN_SPEED*2
		player.position.x = clamp(new_position.x, 0, OS.get_window_size().x)
		player.position.y = clamp(new_position.y, 0, OS.get_window_size().y)
	broadcast_positions()
	
func broadcast_positions():
	for player in $PlayersContainer.get_children():
		for other in $PlayersContainer.get_children():
			rpc_unreliable_id( int(player.get_name()), "update_position", other.get_name(), other.position)
		rpc_unreliable_id( int(player.get_name()), "update_ball", currentBall.position, currentBall.linear_velocity)

func resetNewBall():
	if currentBall != null:
		remove_child(currentBall)
		currentBall.queue_free()
	currentBall = ballNode.duplicate()
	add_child(currentBall)
	
func _on_Area2DUP_body_entered(body):
	if body != currentBall:
		return
	resetNewBall()

func _on_Area2DRIGHT_body_entered(body):
	if body != currentBall:
		return
	resetNewBall()

func _on_Area2DBOTTOM_body_entered(body):
	if body != currentBall:
		return
	resetNewBall()

func _on_Area2DLEFT_body_entered(body):
	if body != currentBall:
		return
	resetNewBall()
